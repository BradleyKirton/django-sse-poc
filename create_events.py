import asyncio
import datetime
import json

import aioredis


async def create_event() -> None:
    """Helper coroutine to create a SSE."""
    now = datetime.datetime.now()
    redis = await aioredis.create_redis_pool("redis://localhost")
    payload = json.dumps({"message": now.isoformat()})
    await redis.publish("channel:sse", payload)
    redis.close()


if __name__ == "__main__":
    asyncio.run(create_event())
