# Django SSE POC

This project serves as a proof of concept for using server sent events within Django 3.

## Installation

The project makes use of redis for pubsub so you will need to install that on your OS before continuing.

```bash
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Running

The project includes a Makefile to help you run it.

```bash
make run -j2
```

The above command will run redis-server and Django. Once running navigate to `http://localhost:8000` in your browser.

Now you can generate events by publishing onto the `channel:sse` redis channel. There is a helper script `create_events.py` which can be run to do this. For example:

```bash
python create_events.py
```