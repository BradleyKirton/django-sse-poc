redis:
	@redis-server

serve:
	@uvicorn config.asgi:application

run: redis serve