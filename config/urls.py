import asyncio
import json
from typing import Union

import aioredis
from asgiref.sync import async_to_sync
from django import forms
from django.contrib import admin
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import path, reverse
from django.views.decorators.http import require_http_methods


@async_to_sync
async def publish_message(message: str) -> None:
    redis = await aioredis.create_redis_pool("redis://localhost")
    await redis.publish("channel:sse", json.dumps({"message": message}))
    redis.close()


class MessageForm(forms.Form):
    message = forms.CharField()


@require_http_methods(["GET", "POST"])
def message_view(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """Serve the index.html page."""

    if request.method == "GET":
        return render(
            request,
            "form.html",
            context={"form": MessageForm()},
        )

    form = MessageForm(request.POST)
    if not form.is_valid():
        return HttpResponseRedirect(reverse("message-view"))

    publish_message(form.cleaned_data["message"])
    return HttpResponseRedirect(reverse("message-view"))


def index(request: HttpRequest) -> HttpResponse:
    """Serve the index.html page."""

    return render(request, "index.html")


urlpatterns = [
    path("admin/", admin.site.urls),
    path("", index),
    path("message/", message_view, name="message-view"),
]
